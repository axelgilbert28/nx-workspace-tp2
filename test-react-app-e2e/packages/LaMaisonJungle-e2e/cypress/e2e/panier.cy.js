describe('empty spec', () => {
  beforeEach(() => {
    cy.visit('/')
    cy.location('pathname', { timeout: 10000 }).should('eq', '/')
  })
  it('passes', () => {
    //test l'ajout de chaque article au panier
    cy.get('.lmj-layout-inner')
      .children('.lmj-shopping-list')
      .children('ul')
      .children('div')
      .find('button')
      .should('exist')
      .should('be.visible')
      .click({ multiple: true })

    //on va tester la fermeture et l'ouverture du panier
    cy.get('.lmj-layout-inner')
      .children('.lmj-cart')
      .find('button:first')
      .should('exist')
      .should('be.visible')
      .click() //test la fermeture du panier
      .click() //test l'ouverture du panier

      //on va tester si il y a des articles au panier
      cy.get('.lmj-layout-inner')
      .children('.lmj-cart')
      .children('div')
      .children('ul')
      .children('div')
      .should('exist')

      //on va tester de vider le panier
      cy.get('.lmj-layout-inner')
      .children('.lmj-cart')
      .children('div')
      .find('button:first')
      .should('exist')
      .should('be.visible')
      .click() //vide le panier

      //mtn que panier vide > tester si les articles ont été supp du panier
       cy.get('.lmj-layout-inner')
       .children('.lmj-cart')
       .children('div')
       .contains('Votre panier est vide')
  })
})